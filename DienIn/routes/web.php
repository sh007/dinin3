<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/


Route::get('/',"UserController@Login");
Route::post('UserLoginPost',"UserController@UserLoginPost");
Route::get('/UserLogout',"UserController@UserLogout");


Route::get('/Index',"IndexController@Index");
Route::get('GetCategoryItem',"IndexController@GetCategoryItem");

Route::get('AddToCart',"IndexController@AddToCart");
Route::get('ItemRemove',"IndexController@ItemRemove");
Route::get('CartDestroy',"IndexController@CartDestroy");
Route::get('UpdateQty',"IndexController@UpdateQty");
Route::get('LoadCartdata',"IndexController@LoadCartdata");
