@extends('main')

@section('myContent')

    <div class="row justify-content-center" id="loginForm">
        
        <div class="col-md-4">
            <form action="UserLoginPost" method="POST">
                <input type="hidden" name="_token" value="<?php echo csrf_token();?>"/>
                <h3>Login</h3>
                <label>User Name</label>
                <input type="text" name="userName" class="form-control"/>
                <label>Password</label>
                <input type="password" name="password"  class="form-control"/>
                <br/>
                <input type="submit" value="Login" class="btn btn-success btn-block"/>
            </form>
        </div>

    </div>

@stop