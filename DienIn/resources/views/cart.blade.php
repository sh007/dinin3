<table class="table tblitem" style="margin-bottom:0">
               <thead>
                    <tr>
                         <th><i class="fa fa-user-circle fa-2x" aria-hidden="true"></i> Javed Akhtar</th>
                         <th>#ORD-4582</th>
                         
                         <th><i class="fa fa-print" aria-hidden="true"></i> <i class="fa fa-th-list" aria-hidden="true"></i></th>
                    </tr>
               <thead>
               <tbody>
                         
                    @foreach(Cart::content() as $row)
                         <tr class="itemDetail">
                              <td >
                                   <i class="fa fa-times itemRemove cis" aria-hidden="true" onclick="itemRemove('{{$row->rowId}}')"></i> 
                                   {{$row->name}}
                              </td>

                              <td>{{$row->qty}} 
                                   <i class="fa fa-plus-circle cis" aria-hidden="true" onclick="updateQty({{$row->qty+1}},'{{$row->rowId}}')"></i> 
                                   <i class="fa fa-minus-circle cis" aria-hidden="true" onclick="updateQty({{$row->qty-1}},'{{$row->rowId}}')"></i>
                              </td>
                             
                              <td>{{$row->total}}</td>
                         </tr>
                    @endforeach
                   
               </tbody>     
          </table>
          <table class="table">
               <thead></thead>
               <tbody>
                    <tr><td colspan="2">Sub Total</td> <td>{{ Cart::subtotal()}}</td></tr>
                    <tr><td colspan="2">Tax</td> <td>{{ Cart::tax()}}</td></tr>
                    <tr><td colspan="2">Total</td> <td>{{ Cart::total()}}</td></tr>
               </tbody>
          </table>


          <input type="button" class="btn btn-success" onclick="cartDestroy()" value="Empty cart"/><br><br>
          <input type="button" class="btn btn-success btn-block"  value="Pay"/>