@extends('main')


@section('myContent')

<div class="row" style="margin: 1%;" >
     <!--Category List--->
     <div class="col-md-2">
     <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
          <span class="btn btn-success">
               <i class="fa fa-address-card" aria-hidden="true"></i> {{session()->get('userName')}}
               <a href="/UserLogout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
          </span>
     </div>
     <div class="col-md-10" id="ItemCategory">
    
          <ul>
               @foreach($CategoryList as $categoryList)
               <li catId="{{$categoryList->catId}}">{{$categoryList->catName}}</li>
               @endforeach

          </ul>
     </div>
     <!--end Category List--->
</div>

<div class="row" style="margin:1%">
     
     <div class="col-md-5">
      <!--Cart Table--->
          <div class="cartData">
                @include('cart')
           </div>
     <!--end Cart Table--->
     </div>
     <div class="col-md-7" >
      <!--Search Text Box--->
          <div class="input-group mb-3">
               <input type="text" class="form-control" placeholder="Search Item"  aria-describedby="basic-addon2" id="searchi">
               <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-search" aria-hidden="true"></i></span>
               </div>
          </div>
     <!-- end Search Text Box--->

     <!--Item List--->
          <div  id="itemList" >
               <ul>
               
               </ul>
          </div>
     <!-- end Item List--->    
     </div>
</div>


@stop

@section('myScript')



<script type="text/javascript">

     $(document).ready(function(){
          /***convert all item in json **/
          var allItem=<?php echo json_encode($AllItemList); ?>;

            /***call function for Load all item on load **/
          ShowFirstCatItems();

          /***Load all item function **/    
          function ShowFirstCatItems(){
               $("#ItemCategory li").first().addClass('listSelected');
               var categoryId=1
               var myItemList="";
               $.each(allItem,function(index,value){
                    if(1==1){
                         myItemList+="<li itemId="+value.itemId+" class="+(value.isVegNonVeg ==1 ? 'veg':'NonVeg' )+">"+value.itemName +"</li>"; 
                    }
               });
              
               $("#itemList ul").html(myItemList);
          }

          //for show selected category item / category click
          $(document).on("click","#ItemCategory ul li",function(){

               $("#ItemCategory li").removeClass('listSelected');

              $(this).addClass('listSelected');

               var categoryId=$(this).attr("catId");
               var myItemList="";
               $.each(allItem,function(index,value){
                    if(value.catId==categoryId){
                         myItemList+="<li itemId="+value.itemId+" class="+(value.isVegNonVeg ==1 ? 'veg':'NonVeg' )+">"+value.itemName +value.isVegNonVeg+"</li>"; 
                    }
               });
               $("#itemList ul").html(myItemList);
          });
     });
     
       /***on item click action **/
     $(document).on("click","#itemList ul li",function(){

          $.ajax({
               url:'/AddToCart',
               method:'GET',
               data:{itemId:$(this).attr("itemId")},
               success:function(data){
                   // console.log(data);
                    $(".cartData").html(data);
               }
          });
     });
      
       /***Item remove function **/
     function itemRemove(itemRowId) {
 
          $.ajax({
            url:'/ItemRemove',
            method:'GET',
            data:{itemRowId:itemRowId},
            success:function(data){
                $(".cartData").html(data);
               
            }    
        });
     }

      /***cart destroy function **/
     function cartDestroy()
     {
          $.ajax({
            url:'/CartDestroy',
            method:'GET',
            data:{},
            success:function(data){
                // console.log(data);
               location.reload();
               
            }    
        });
     }
      /***update item qty function **/
     function updateQty(qty,rowId)
     {
          $.ajax({
            url:'/UpdateQty',
            method:'GET',
            data:{qty:qty,rowId:rowId},
            success:function(data){
              
              // location.reload();
             // $(".table").load(location.href + "/")
             $(".cartData").html(data);
            }    
          });
     }

     /**search textbox Key Press event **/
     /*$("#searchi").keypress(function(){
        var j=this.value;
          console.log(j);
     });*/
     $('#searchi').on("input", function() {
          var searchValue = this.value;
          $("#itemList ul li:not(:contains("+searchValue+"))").css("display", "none");

          $("#itemList ul li:contains("+searchValue+")").css("display", "unset");

   
      
     });
</script>

@stop