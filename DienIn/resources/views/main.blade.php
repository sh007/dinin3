<html>
<head>
    <title></title>
    <link href="{{asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{asset('css/mystyle.css') }}" rel="stylesheet">
    <style>
body {
  font-family: "Lato", sans-serif;
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
</head>
<body >

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>




@yield('myContent')

</body>

</html>
<script src="{{asset('js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.easing.min.js')}}"></script>

@yield('myScript')

<script type="text/javascript">
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

  $(document).ready(function(){
 


    /* $(document).on("click","#ItemCategory ul li",function(){
        $.ajax({
            url:'/GetCategoryItem',
            method:'GET',
            data:{catId:$(this).attr("catId")},
            success:function(data){

              var myItemList="";
              $.each(data,function(index,value){
                 myItemList+="<li itemId="+value.itemId+">"+value.itemName+"</li>"; 
                
              });
              $("#itemList ul").html(myItemList);
               
            }    
        });
    });*/

    
  });
</script>


