<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Cart;

class IndexController extends Controller
{
   public function Index()
   {
      if(session()->has('userName')){
        $categoryList=DB::select('SELECT * FROM tblcategory');
        $allItemList= DB::select('SELECT * FROM tblitem');
        return view('Index',['CategoryList'=>$categoryList,'AllItemList'=>$allItemList]);
      }
      else{
        return redirect('/');
      }
     

   }
 /*  public function GetCategoryItem(Request $req)
   {
     $catId=$req->input('catId');

     $result= DB::select('SELECT * FROM tblitem WHERE catId=?',[$catId]);
  
     return $result;
   }*/
  

   public function AddToCart(Request $req){
 
      $itemId=$req->input('itemId');
      $result= DB::select('SELECT * FROM tblitem WHERE itemId=?',[$itemId]);

      foreach($result as $item){
        Cart::add($item->itemId, $item->itemName, 1, $item->price);
      } 
      return view('cart');
   }

   public function ItemRemove(Request $req)
   {
     $itemRowId=$req->input('itemRowId');

        Cart::remove($itemRowId);
        return view("cart");

   }
   public function CartDestroy()
   {
    
    Cart::destroy();

  
   }
   public function UpdateQty(Request $req)
   {
      Cart::update($req->input('rowId'), $req->input('qty'));
      return view('cart');
   }
   public function LoadCartdata(){
     return view('cart');
   }
}
