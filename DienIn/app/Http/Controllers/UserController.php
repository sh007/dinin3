<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function Login()
    {
       return view('user.login');
    }
    public function UserLoginPost(Request $req){
       $username=$req->input('userName');
       $password=$req->input('password');

      $result=DB::select('select * from tbluser where userName=? and password=?',[$username,$password]);

      if(count($result)>0){
        foreach($result as $user){
            session()->put('userName', $user->userName);
            session()->put('userId', $user->id);
            return redirect('Index');
        }
      }
      else{
          echo "invalid username ans password";
      }
    }

    public function UserLogout(){
        session()->flush();
        return redirect('/');
    }
}
