-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2019 at 05:27 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dinein`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblcategory`
--

CREATE TABLE `tblcategory` (
  `catId` int(2) NOT NULL,
  `catName` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcategory`
--

INSERT INTO `tblcategory` (`catId`, `catName`) VALUES
(1, 'Hari Sabji'),
(2, 'Dal'),
(3, 'Chiken'),
(4, 'Roti'),
(5, 'Pizza'),
(6, 'Pizza2'),
(7, 'Pizza3'),
(8, 'Pizza24'),
(9, 'Pizza5'),
(10, 'Pizza64'),
(11, 'Pizza8'),
(12, 'Pizza29'),
(13, 'Pizza8'),
(14, 'Pizza29'),
(15, 'Pizza8'),
(16, 'Pizza29'),
(17, 'Pizza8'),
(18, 'Pizza29');

-- --------------------------------------------------------

--
-- Table structure for table `tblitem`
--

CREATE TABLE `tblitem` (
  `itemId` int(3) NOT NULL,
  `catId` int(2) NOT NULL,
  `itemName` varchar(30) NOT NULL,
  `qty` varchar(10) NOT NULL,
  `price` float NOT NULL,
  `isVegNonVeg` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblitem`
--

INSERT INTO `tblitem` (`itemId`, `catId`, `itemName`, `qty`, `price`, `isVegNonVeg`) VALUES
(1, 1, 'Gobhi Aloo javed ali', 'Half', 50, 0),
(2, 1, 'Palak Aloo', 'Half', 80, 0),
(3, 2, 'Dal Tadka', 'Half', 30, 1),
(4, 1, 'sdfsetertre', '5', 10, 1),
(5, 1, 'sdfsetertre', '5', 10, 1),
(6, 1, 'sdfsetertre', '5', 10, 1),
(7, 1, 'sdfsetertre', '5', 10, 1),
(8, 1, 'sdfsetertre', '5', 10, 1),
(9, 1, 'sdfsetertre', '5', 10, 1),
(10, 1, 'sdfsetertre', '5', 10, 1),
(11, 1, 'sdfsetertre', '5', 10, 1),
(12, 1, 'sdfsetertre', '5', 10, 1),
(13, 1, 'sdfsetertre', '5', 10, 1),
(14, 1, 'sdfsetertre', '5', 10, 1),
(15, 1, 'sdfsetertre', '5', 10, 1),
(16, 1, 'sdfsetertre', '5', 10, 1),
(17, 1, 'sdfsetertre', '5', 10, 1),
(18, 1, 'sdfsetertre', '5', 10, 1),
(19, 1, 'sdfsetertre', '5', 10, 1),
(26, 1, 'sdfsetertre', '5', 10, 1),
(27, 1, 'sdfsetertre', '5', 10, 1),
(28, 1, 'sdfsetertre', '5', 10, 1),
(29, 1, 'sdfsetertre', '5', 10, 1),
(30, 1, 'sdfsetertre', '5', 10, 1),
(31, 1, 'sdfsetertre', '5', 10, 1),
(32, 1, 'sdfsetertre', '5', 10, 1),
(33, 1, 'sdfsetertre', '5', 10, 1),
(34, 1, 'sdfsetertre', '5', 10, 1),
(35, 1, 'sdfsetertre', '5', 10, 1),
(36, 1, 'sdfsetertre', '5', 10, 1),
(37, 1, 'sdfsetertre', '5', 10, 1),
(38, 1, 'sdfsetertre', '5', 10, 1),
(39, 1, 'sdfsetertre', '5', 10, 1),
(40, 1, 'sdfsetertre', '5', 10, 1),
(41, 1, 'sdfsetertre', '5', 10, 1),
(42, 1, 'sdfsetertre', '5', 10, 1),
(43, 1, 'sdfsetertre', '5', 10, 1),
(44, 1, 'sdfsetertre', '5', 10, 1),
(45, 1, 'sdfsetertre', '5', 10, 1),
(46, 1, 'sdfsetertre', '5', 10, 1),
(47, 1, 'sdfsetertre', '5', 10, 1),
(48, 1, 'sdfsetertre', '5', 10, 1),
(49, 1, 'sdfsetertre', '5', 10, 1),
(50, 1, 'sdfsetertre', '5', 10, 1),
(51, 1, 'sdfsetertre', '5', 10, 1),
(52, 1, 'sdfsetertre', '5', 10, 1),
(53, 1, 'sdfsetertre', '5', 10, 1),
(54, 1, 'sdfsetertre', '5', 10, 1),
(55, 1, 'sdfsetertre', '5', 10, 1),
(56, 1, 'sdfsetertre', '5', 10, 1),
(57, 1, 'sdfsetertre', '5', 10, 1),
(58, 1, 'sdfsetertre', '5', 10, 1),
(59, 1, 'sdfsetertre', '5', 10, 1),
(60, 1, 'sdfsetertre', '5', 10, 1),
(61, 1, 'sdfsetertre', '5', 10, 1),
(62, 1, 'sdfsetertre', '5', 10, 1),
(63, 1, 'sdfsetertre', '5', 10, 1),
(64, 1, 'sdfsetertre', '5', 10, 1),
(65, 1, 'sdfsetertre', '5', 10, 1),
(66, 1, 'sdfsetertre', '5', 10, 1),
(67, 1, 'sdfsetertre', '5', 10, 1),
(68, 1, 'sdfsetertre', '5', 10, 1),
(69, 1, 'sdfsetertre', '5', 10, 1),
(70, 1, 'sdfsetertre', '5', 10, 1),
(71, 1, 'sdfsetertre', '5', 10, 1),
(72, 1, 'sdfsetertre', '5', 10, 1),
(73, 1, 'sdfsetertre', '5', 10, 1),
(74, 1, 'sdfsetertre', '5', 10, 1),
(75, 1, 'sdfsetertre', '5', 10, 1),
(76, 1, 'sdfsetertre', '5', 10, 1),
(77, 1, 'sdfsetertre', '5', 10, 1),
(78, 1, 'sdfsetertre', '5', 10, 1),
(79, 1, 'sdfsetertre', '5', 10, 1),
(80, 1, 'sdfsetertre', '5', 10, 1),
(81, 1, 'sdfsetertre', '5', 10, 1),
(82, 1, 'sdfsetertre', '5', 10, 1),
(83, 1, 'sdfsetertre', '5', 10, 1),
(84, 1, 'sdfsetertre', '5', 10, 1),
(85, 1, 'sdfsetertre', '5', 10, 1),
(86, 1, 'sdfsetertre', '5', 10, 1),
(87, 1, 'sdfsetertre', '5', 10, 1),
(88, 1, 'sdfsetertre', '5', 10, 1),
(89, 1, 'sdfsetertre', '5', 10, 1),
(90, 1, 'sdfsetertre', '5', 10, 1),
(91, 1, 'sdfsetertre', '5', 10, 1),
(92, 1, 'sdfsetertre', '5', 10, 1),
(93, 1, 'sdfsetertre', '5', 10, 1),
(94, 1, 'sdfsetertre', '5', 10, 1),
(95, 1, 'sdfsetertre', '5', 10, 1),
(96, 1, 'sdfsetertre', '5', 10, 1),
(97, 1, 'sdfsetertre', '5', 10, 1),
(98, 1, 'sdfsetertre', '5', 10, 1),
(99, 1, 'sdfsetertre', '5', 10, 1),
(100, 1, 'sdfsetertre', '5', 10, 1),
(101, 1, 'sdfsetertre', '5', 10, 1),
(102, 1, 'sdfsetertre', '5', 10, 1),
(103, 1, 'sdfsetertre', '5', 10, 1),
(104, 1, 'sdfsetertre', '5', 10, 1),
(105, 1, 'sdfsetertre', '5', 10, 1),
(106, 1, 'sdfsetertre', '5', 10, 1),
(107, 1, 'sdfsetertre', '5', 10, 1),
(108, 1, 'sdfsetertre', '5', 10, 1),
(109, 1, 'sdfsetertre', '5', 10, 1),
(110, 1, 'sdfsetertre', '5', 10, 1),
(111, 1, 'sdfsetertre', '5', 10, 1),
(112, 1, 'sdfsetertre', '5', 10, 1),
(113, 1, 'sdfsetertre', '5', 10, 1),
(114, 1, 'sdfsetertre', '5', 10, 1),
(115, 1, 'sdfsetertre', '5', 10, 1),
(116, 1, 'sdfsetertre', '5', 10, 1),
(117, 1, 'sdfsetertre', '5', 10, 1),
(118, 1, 'sdfsetertre', '5', 10, 1),
(119, 1, 'sdfsetertre', '5', 10, 1),
(120, 1, 'sdfsetertre', '5', 10, 1),
(121, 1, 'sdfsetertre', '5', 10, 1),
(122, 1, 'sdfsetertre', '5', 10, 1),
(123, 1, 'sdfsetertre', '5', 10, 1),
(124, 1, 'sdfsetertre', '5', 10, 1),
(125, 1, 'sdfsetertre', '5', 10, 1),
(126, 1, 'sdfsetertre', '5', 10, 1),
(127, 1, 'sdfsetertre', '5', 10, 1),
(128, 1, 'sdfsetertre', '5', 10, 1),
(129, 1, 'sdfsetertre', '5', 10, 1),
(130, 1, 'sdfsetertre', '5', 10, 1),
(131, 1, 'sdfsetertre', '5', 10, 1),
(132, 1, 'sdfsetertre', '5', 10, 1),
(133, 1, 'sdfsetertre', '5', 10, 1),
(134, 1, 'sdfsetertre', '5', 10, 1),
(135, 1, 'sdfsetertre', '5', 10, 1),
(136, 1, 'sdfsetertre', '5', 10, 1),
(137, 1, 'sdfsetertre', '5', 10, 1),
(138, 1, 'sdfsetertre', '5', 10, 1),
(139, 1, 'sdfsetertre', '5', 10, 1),
(140, 1, 'sdfsetertre', '5', 10, 1),
(141, 1, 'sdfsetertre', '5', 10, 1),
(142, 1, 'sdfsetertre', '5', 10, 1),
(143, 1, 'sdfsetertre', '5', 10, 1),
(144, 1, 'sdfsetertre', '5', 10, 1),
(145, 1, 'sdfsetertre', '5', 10, 1),
(146, 1, 'sdfsetertre', '5', 10, 1),
(147, 1, 'sdfsetertre', '5', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblsaleheader`
--

CREATE TABLE `tblsaleheader` (
  `id` int(5) NOT NULL,
  `orderNo` varchar(12) NOT NULL,
  `customerName` varchar(20) DEFAULT NULL,
  `customerMobileNo` varchar(13) DEFAULT NULL,
  `subTotal` float NOT NULL,
  `tax` float NOT NULL,
  `billTotal` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsaleitemdetail`
--

CREATE TABLE `tblsaleitemdetail` (
  `id` int(5) NOT NULL,
  `sohId` int(5) NOT NULL,
  `itemName` int(5) NOT NULL,
  `itemQty` float NOT NULL,
  `itemPrice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `id` int(3) NOT NULL,
  `userName` varchar(15) NOT NULL,
  `password` varchar(12) NOT NULL,
  `userType` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`id`, `userName`, `password`, `userType`) VALUES
(1, 'javed', '123', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblcategory`
--
ALTER TABLE `tblcategory`
  ADD PRIMARY KEY (`catId`);

--
-- Indexes for table `tblitem`
--
ALTER TABLE `tblitem`
  ADD PRIMARY KEY (`itemId`),
  ADD KEY `catId` (`catId`);

--
-- Indexes for table `tblsaleheader`
--
ALTER TABLE `tblsaleheader`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblsaleitemdetail`
--
ALTER TABLE `tblsaleitemdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblcategory`
--
ALTER TABLE `tblcategory`
  MODIFY `catId` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tblitem`
--
ALTER TABLE `tblitem`
  MODIFY `itemId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `tblsaleheader`
--
ALTER TABLE `tblsaleheader`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblsaleitemdetail`
--
ALTER TABLE `tblsaleitemdetail`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblitem`
--
ALTER TABLE `tblitem`
  ADD CONSTRAINT `tblitem_ibfk_1` FOREIGN KEY (`catId`) REFERENCES `tblcategory` (`catId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
